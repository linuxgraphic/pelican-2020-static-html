var escapeHTML = (function() {
	var escapeChars = {
		'¢' : 'cent',
		'£' : 'pound',
		'¥' : 'yen',
		'€': 'euro',
		'©' :'copy',
		'®' : 'reg',
		'<' : 'lt',
		'>' : 'gt',
		'"' : 'quot',
		'&' : 'amp',
		'\'' : '#39'
	};

	var regex = new RegExp( '['+ Object.keys(escapeChars).join('') + ']', 'g');

	return function(str) {
    return str.replace(regex, function(m) {
      return '&' + escapeChars[m] + ';';
    });
  }
})()

var unescapeHTML = (function() {
	var escapeChars = {
		'cent' : '¢',
		'pound' : '£',
		'yen' : '¥',
		'euro': '€',
		'copy' :'©',
		'reg' : '®',
		'lt' : '<',
		'gt' : '>',
		'quot' : '"',
		'amp' : '&',
		'#39' : '\''
	};

	var regex = new RegExp( '&('+ Object.keys(escapeChars).join('|') + ');', 'g');

	return function(str) {
    return str.replace(regex, function(m, p1) {
      return escapeChars[p1];
    });
  }
})()

function Feed(url) {
  this.url = url;
  this.outputDomItemName = null;
  this.outputShowTitle = null;
  this.templateDomItemName = null;
  this.fillTemplate = null;
};

Feed.prototype.output = function(domItemName) {
  this.outputDomItemName = domItemName;
  return this;
}

Feed.prototype.template = function(domItemName) {
  this.templateDomItemName = domItemName;
  return this;
}

Feed.prototype.fill = function(fn) {
  this.fillTemplate = fn;
  return this;
}

Feed.prototype.fetch = function() {
  fetch(this.url).then((res) => this.render(res))
    .catch(() => console.error('Error in fetching the website'))
  return this;
}

Feed.prototype.showTitle = function(show = true) {
  this.outputShowTitle = show;
  return this;
}

/// will be called by fetch on success
Feed.prototype.render = function(response) {
  response.text().then((htmlTxt) => {
    const dateFormat = { weekday: 'short', month: 'short', day: 'numeric' };
    const timeFormat = { hour: '2-digit', minute: '2-digit' };

    var htmlFragment = document.createDocumentFragment()

    var domParser = new DOMParser()
    const xmlDoc = domParser.parseFromString(htmlTxt, 'text/xml')
    const xmlEntries = xmlDoc.getElementsByTagName('entry');
    for (const xmlEntry of xmlEntries) {
      let entry = {'author': '', 'updated': '', 'forum': '', 'title': '', 'url': '', 'content': ''};
      for (const field of xmlEntry.childNodes) {
        if (!field['localName']) {
          continue;
        }
        const fieldName = field.localName;
        if (fieldName == 'author') {
          entry.author = field.textContent;
        } else if (fieldName == 'updated') {
          const updated = new Date(field.textContent);
          // console.log(theDate.toLocaleDateString('fr-FR', dateFormat));
          // console.log(theDate.toLocaleTimeString('fr-FR', timeFormat));
          entry.updated = updated.toLocaleDateString('fr-FR', dateFormat) + ', ' + updated.toLocaleTimeString('fr-FR', timeFormat)
        } else if (fieldName == 'title') {
            if (field.textContent.includes(' • ')) {
              title = field.textContent.split(' • ');
              entry.title = title[1];
            } else {
              entry.title = field.textContent;
            }
        } else if (fieldName == 'id') {
          entry.url = field.textContent;
        } else if (fieldName == 'content') {
          entry.content = field.textContent;
        } else {
          // console.log(fieldName, field.textContent);
        }
        // console.log(field['localName']);
      }
      let template = document.importNode(document.querySelector(this.templateDomItemName).content, true);
      let t = template.querySelector.bind(template)
      if (this.fillTemplate) {
        this.fillTemplate(t, entry);
      } else {
        t('strong').textContent = unescapeHTML(entry.title);
        t('a').href = entry.url;
        t('span').textContent =  entry.author + ', ' + entry.updated;
      }
      htmlFragment.appendChild(template)
      // const authorXml = entry.getElementsByTagName('entry');
      // const author = entry.firstElementChild.firstElementChild.
      //
    }
    htmlOutput = document.querySelector(this.outputDomItemName)
      while (htmlOutput.firstChild) {
          htmlOutput.removeChild(htmlOutput.lastChild);
      }
    var htmlUl = document.createElement("ul");
    htmlUl.appendChild(htmlFragment);
    htmlOutput.appendChild(htmlUl);
  })
  return this;
}
